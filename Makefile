# 指定使用的编译器(根据项目情况修改)
CC = /usr/local/arm/gcc-linaro-4.9.4-2017.01-x86_64_arm-linux-gnueabihf/bin/arm-linux-gnueabihf-gcc

# 生成的可执行文件名(根据项目情况修改)
TARGET = linux_gpio_submodule_demo

# --------------------以下为指定编译器参数(根据项目情况修改)--------------------
# -std=gnu99: 支持C99标准 这里不要使用-std=c99, 否则会报警告提示部分库函数未包含对应头文件
# -Wall: 打开所有警告信息
# -O0: 不优化代码
CFLAGS := -std=gnu99 -Wall -O0

# --------------------以下为指定头文件路径(根据项目情况修改)--------------------
# CFLAGS += -I$(TOPDIR)/staging_dir/target-mipsel_24kec+dsp_uClibc-0.9.33.2/usr/include/
# CFLAGS += -I./easy_logger/inc  -I./easy_logger/plugins/

# 链接器的链接参数设置(根据项目情况修改)
# --------------------以下为指定头文件路径(根据项目情况修改)--------------------
# pthread不是linux下的默认的库，如果这里不加-lpthread，则在链接的时候无法找到phread库中各函数的入口地址，链接会失败
# LDFLAGS := -lm
# LDFLAGS += -L$(TOPDIR)/staging_dir/target-mipsel_24kec+dsp_uClibc-0.9.33.2/usr/lib/

# 获取各工程文件所在的相对目录(根据项目情况修改)
TOP_PATH = .
GPIO_PATH = ./gpio
USER_GPIO_PATH = ./user_gpio

# 获取工程中各文件夹下的源文件(.c)、目标文件(.o)的文件列表(根据项目情况修改)
# 获取当前目录下的所有的.c文件列表
TOP_SRC = $(wildcard $(TOP_PATH)/*.c)
# 将列表中所有文件名的后缀.c替换为.o, 这样就可以得到在当前目录可生成的.o文件列表,  patsubst ：替换通配符
TOP_OBJ = $(patsubst %.c, %.o, $(TOP_SRC))

GPIO_SRC = $(wildcard $(GPIO_PATH)/*.c)
GPIO_OBJ = $(patsubst %.c, %.o, $(GPIO_SRC))

USER_GPIO_SRC = $(wildcard $(USER_GPIO_PATH)/*.c)
USER_GPIO_OBJ = $(patsubst %.c, %.o, $(USER_GPIO_SRC))

# 所有目标文件的集合(根据项目情况修改)
TARGET_OBJS := $(TOP_OBJ) $(GPIO_OBJ) $(USER_GPIO_OBJ)

# --------------------以下为编译默认目标规则(不需要修改)--------------------
# Makefile的默认目标
$(TARGET):$(TARGET_OBJS)
	$(CC) $^ $(CFLAGS) $(LDFLAGS) -o $(TARGET)
	@echo "compile done, target file: $(TARGET)"

# --------------------以下为清除规则(不需要修改)--------------------
# 清理目标设置为伪目标，防止外面有clean文件 阻止执行clean
.PHONY:clean
clean:
	-$(RM) $(TARGET_OBJS)
	@echo "clean object file done!"

# 清理目标设置为伪目标
.PHONY:distclean
distclean:
	-$(RM) $(TARGET) $(TARGET_OBJS)
	@echo "clean target and object file done!"
