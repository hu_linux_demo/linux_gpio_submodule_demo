/**
 * @file      : main.c
 * @brief     : 程序入口文件
 * @author    : huenrong (huenrong1028@gmail.com)
 * @date      : 2021-03-21 17:44:20
 * @author    : huenrong
 *
 * @copyright : Copyright (c) 2021 胡恩荣
 *
 */

#include <stdio.h>
#include <unistd.h>

#include "./user_gpio/user_gpio.h"

/**
 * @brief  : 程序入口
 * @param  : argc: 参数个数
 * @param  : argv: 参数列表
 * @return : 成功: 0
 *           失败: 其它
 */
int main(int argc, char *argv[])
{
    int ret = -1;

    // LED初始化
    ret = led_init();
    if (0 != ret)
    {
        perror("led init fail");

        return -1;
    }

    // BEEP初始化
    ret = beep_init();
    if (0 != ret)
    {
        perror("beep init fail");

        return -1;
    }

    // KEY 初始化
    ret = key_init_with_poll();
    if (0 != ret)
    {
        perror("key init fail");

        return -1;
    }

    while (1)
    {
        enum gpio_value key_value;

        ret = read_key_value_with_poll(&key_value);
        if (0 == ret)
        {
            // 按键按下
            if ((GPIO_LOW == key_value))
            {
                LED_ON();
                BEEP_ON();
            }
            // 按键释放
            else
            {
                LED_OFF();
                BEEP_OFF();
            }
        }
    }

    return 0;
}
