/**
 * @file      : user_gpio.h
 * @brief     : 用户GPIO控制头文件
 * @author    : huenrong (huenrong1028@gmail.com)
 * @date      : 2021-03-23 21:01:22
 * @author    : huenrong
 *
 * @copyright : Copyright (c) 2021 胡恩荣
 *
 */

#ifndef __USER_GPIO_H
#define __USER_GPIO_H

#include <stdint.h>

#include "../gpio/gpio.h"

// GPIO引脚定义
// LED GPIO引脚
#define GPIO_LED 3
// BEEP GPIO引脚
#define GPIO_BEEP 129
// KEY GPIO引脚
#define GPIO_KEY 18

// LED状态控制
#define LED_ON() gpio_set_value(GPIO_LED, GPIO_LOW)
#define LED_OFF() gpio_set_value(GPIO_LED, GPIO_HIGH)

// BEEP状态控制
#define BEEP_ON() gpio_set_value(GPIO_BEEP, GPIO_LOW)
#define BEEP_OFF() gpio_set_value(GPIO_BEEP, GPIO_HIGH)

/**
 * @brief  LED初始化
 * @return 成功: 0
 *         失败: -1
 */
int led_init(void);

/**
 * @brief  BEEP初始化
 * @return 成功: 0
 *         失败: -1
 */
int beep_init(void);

/**
 * @brief  KEY初始化
 * @return 成功: 0
 *         失败: -1
 */
int key_init(void);

/**
 * @brief  KEY初始化(使用poll方式读取键值)
 * @return 成功: 0
 *         失败: -1
 */
int key_init_with_poll(void);

/**
 * @brief  LED 闪烁
 * @param  ms: 输入参数, 点亮时间(单位: ms)
 */
void led_blinking(const uint16_t ms);

/**
 * @brief  BEEP响
 * @param  ms: 输入参数, 时间(单位: ms)
 */
void beep_on(const uint16_t ms);

/**
 * @brief  BEEP循环响
 * @param  num: 输入参数, 蜂鸣器响的次数
 * @param  ms : 输入参数, 蜂鸣器单次响的时间(单位: ms)
 */
void beep_on_num(const uint16_t num, const uint16_t ms);

/**
 * @brief  读取按键键值
 * @param  value: 输出参数, 读取到的按键键值
 * @return 成功: 0
 *         失败: -1
 */
int read_key_value(enum gpio_value *value);

/**
 * @brief  poll方式读取按键键值
 * @param  value: 输出参数, 读取到的按键键值
 * @return 成功: 0
 *         失败: -1
 *         超时: -2
 */
int read_key_value_with_poll(enum gpio_value *value);

#endif // __USER_GPIO_H
